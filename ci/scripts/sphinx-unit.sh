#!/usr/bin/env bash
# vi :set ft=bash:

set -euo pipefail

docsite="$(pwd)/docsite"
dochtml="$(pwd)/dochtml"

pushd "${docsite}" > /dev/null 2>&1

make BUILDDIR="${dochtml}" linkcheck

popd > /dev/null 2>&1
