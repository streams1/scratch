#!/usr/bin/env bash
# vi :set ft=bash:

set -euo pipefail

if ! command -v fly >/dev/null 2>&1; then
  echo "fly CLI required." && exit 1
fi

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
readonly DIR

CONCOURSE_URL="$(grep CONCOURSE_EXTERNAL_URL /opt/concourse_ci/docker-compose.yml | cut -d: -f2- | xargs)"
readonly CONCOURSE_URL

fly -t main login -c "${CONCOURSE_URL}" -u test -p test
fly -t main set-pipeline --non-interactive --pipeline sphinx-pipeline --config "${DIR}/../pipeline/docsite.yml"
fly -t main unpause-pipeline --pipeline sphinx-pipeline
